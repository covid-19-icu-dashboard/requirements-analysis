# Requirements
In this project, the requirements for a dashboard for triage and treatment of COVID-19 associated ventilations will be collected together with experienced intensive care physicians.
The goal is to create a simple and fast solution using modern development technologies.

## [Wiki](https://gitlab.com/covid-19-icu-dashboard/requirements-analysis/-/wikis/home)